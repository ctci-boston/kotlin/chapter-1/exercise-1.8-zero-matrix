import java.lang.IllegalArgumentException

class Matrix<T>(private val matrix: Array<Array<T>>) {
    val numberOfRows = matrix.size
    val numberOfCols = if (numberOfRows > 0) matrix[0].size else 0

    init {
        // Ensure the matrix is regular, MxN, i.e. all the rows have the same size.
        for (i in 0..numberOfRows -1) if (matrix[i].size != numberOfCols) throw IllegalArgumentException()
    }

    fun isEmpty() = numberOfRows == 0
    fun get(row: Int, col: Int): T = matrix[row][col]
    fun set(row: Int, col: Int, value: T) { matrix[row][col] = value }
    fun setRow(row: Int, value: T) { for (i in 0 .. numberOfCols - 1) matrix[row][i] = value }
    fun setCol(col: Int, value: T) { for (i in 0 .. numberOfRows - 1) matrix[i][col] = value }
}

fun zeroMatrix(matrix: Matrix<Int>) {
    val m = matrix.numberOfRows
    val n = matrix.numberOfCols
    val rowSet = mutableSetOf<Int>()
    val colSet = mutableSetOf<Int>()
    fun addToSets(row: Int, col: Int) {
        rowSet.add(row)
        colSet.add(col)
    }

    // Register the zero cells
    for (i in 0..m - 1) for (j in 0..n - 1) if (matrix.get(i, j) == 0) { addToSets(i, j) }
    rowSet.forEach { row -> matrix.setRow(row, 0) }
    colSet.forEach { col -> matrix.setCol(col, 0) }
}
