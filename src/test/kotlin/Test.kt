import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class Test {
    @Test fun `Given an empty matrix verify no changes`() {
        fun intMatrix(m: Int, n: Int): Array<Array<Int>> = Array(m) { Array(n) { 0 } }
        val matrix: Matrix<Int> = Matrix(intMatrix(0, 0))
        zeroMatrix(matrix)
        assertTrue(matrix.isEmpty())
    }

    @Test fun `Given a 1x1 matrix with one non zero cell verify no changes`() {
        fun intMatrix(m: Int, n: Int): Array<Array<Int>> = Array(m) { Array(n) { 1 } }
        val matrix: Matrix<Int> = Matrix(intMatrix(1, 1))
        zeroMatrix(matrix)
        assertFalse(matrix.isEmpty())
        assertEquals(1, matrix.get(0, 0))
    }

    @Test fun `Given a 1x1 matrix with one zero cell verify changes`() {
        fun intMatrix(m: Int, n: Int): Array<Array<Int>> = Array(m) { Array(n) { 1 } }
        val matrix: Matrix<Int> = Matrix(intMatrix(1, 1))
        matrix.set(0, 0, 0)
        zeroMatrix(matrix)
        assertFalse(matrix.isEmpty())
        assertEquals(0, matrix.get(0, 0))
    }

    @Test fun `Given a 3x2 matrix with all non zero cells verify no changes`() {
        fun intMatrix(m: Int, n: Int): Array<Array<Int>> = Array(m) { Array(n) { 1 } }
        val matrix: Matrix<Int> = Matrix(intMatrix(3, 2))
        zeroMatrix(matrix)
        assertFalse(matrix.isEmpty())
        for (i in 0..2) for (j in 0..1) assertEquals(1, matrix.get(i, j))
    }

    @Test fun `Given a 3x2 matrix with one zero cell verify changes`() {
        fun intMatrix(m: Int, n: Int): Array<Array<Int>> = Array(m) { Array(n) { 1 } }
        val matrix: Matrix<Int> = Matrix(intMatrix(3, 2))
        matrix.set(0, 0, 0)
        zeroMatrix(matrix)
        assertFalse(matrix.isEmpty())
        for (i in 0..2) for (j in 0..1) assertEquals(if (i == 0 || j == 0) 0 else 1, matrix.get(i, j))
    }
}
