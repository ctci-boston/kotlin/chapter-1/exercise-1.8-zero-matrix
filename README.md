# 1.8 Zero Matrix

Provide a Kotlin solution for the CtCI Exerise 1.8: Zero Matris: Write an algorithm such that if an element in an MxN matrix is 0, its entire row and column are set to 0.

_Hints_: #17, #74, #702
